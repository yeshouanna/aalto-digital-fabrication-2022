+++
author = "Anna Li"
title = "04-Final Project Portfolio"
date = "2022-05-09"
description = "Instructions for my final project"
tags = [
    "markdown",
    "text",
]
feature_image = "/images/F3-sketch.jpg"
+++

##### Welcome to Cat litter robot instructions! 



#### 1.Project schedule

I want to figure out the worload for my final project, so I give a long-term plan for cat litter robot. I hope that my every week's assginment afterwards could serve for my final project. 

  {{< postimage url = "/images/F5-schedule.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

Depending on my schedule, I probably need two sensors, one is for detect that if cat is there, the other is to avoid the collisions before the scoop moves to the edge. I also need two motors, one is stepper motor which drive the scoop to move, and the other one is a motor for lifting the scoop which is needed to be tested by the servo motor and model. I think that is the next step I need to do firstly. Because I want to improve the scoop which is designed in [Machine building week](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment13/). I want the motor on the sliding rail to be lighter and the speed to be faster, and I think servo motor could do this. 


**Next step from now on**.....


1. **Scoop design and motor selection**. Design the part of the scoop, and motor selection.  test if it is possible to lift part of the scoop, if so, choose to use the servo motor, and design the connection between the servo motor and the scoop.

2. **Sensor selection**. Test several sensors and select the most feasible one to use and determine their position in the box.

3. **Electronic design**. design my pcb board according to the final project and mill them, and programming test.

4. **Appearance consideration**.  Ensure that the whole system is operational and contiue to detailed mechanical design (consider the appearance).

5. **Interface design**.


#### 2.Cat litter robot design(schematics)


* ### Scoop 

  {{< postimage url = "/images/F5-schematic.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/F5-scoop.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  I made a 3D model and waiting for printer

* ### Metal design for the scoop and collector

In the Wildcard week, I just test two metal pieces-scoop and collector. They are pretty good and I decided to make the whole body including litter box, scoop and collector with metal. 

  {{< postimage url = "/images/F4-metal.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


   Sanding  | Bending
--------|------
  {{< postimage url = "/images/F4-sanding.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/F4-bending.jpg" alt ="throwing moon blocks" imgw ="100%">}}


  {{< postimage url = "/images/F4-metal-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


* ### Mechanics of the motors'connection



#### 3.Sensor selection

* ### Weight sensor

The programming test reference is from Kris, he gave me the Arduino tutorial for weight sensor. I decided to follow the link and test the weight sensor. 

1. **How to programm the Weight sensor**, I followed the link:  (https://botland.store/content/161-The-use-of-a-strain-gauge-beam-with-Arduino)

When I started to test weight sensor, every time the reading number is **0** in Arduino serial monitor. I changed the load cell, but still doesn't work. I thought they are broken, then later Matti found the **VDD** PIN which needs to connect power, and the board we had in fablab (Load Cell Amplifier HX711) needs two pins to connect power- VDD/VCC.

  {{< postimage url = "/images/F4-H7.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

2. **Programming**

For the code, I used the **https://github.com/sparkfun/HX711-Load-Cell-Amplifier/blob/master/firmware/SparkFun_HX711_KnownZeroStartup/SparkFun_HX711_KnownZeroStartup.ino**, and I just changed the pin code depending on my board. But firstly, I need set calibration_factor which makes sure your weight sensor reads the right number. So I follow the tutorial to make a simple scale with HX711.The main issue is to make sure that amplifier could be bended on both sides, it means you need to give some space between surface of the scale and load cell. 

   Before  | After
--------|------
  {{< postimage url = "/images/F4-scale-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/F4-scale-2.jpg" alt ="throwing moon blocks" imgw ="100%">}}

**The code**

```

#include "HX711.h"

#define calibration_factor 105000 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  3
#define CLK  2

HX711 scale(DOUT, CLK);

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  Serial.println("Readings:");
}

void loop() {
  Serial.print("Reading: ");
  Serial.print(scale.get_units(), 1); //scale.get_units() returns a float
  Serial.print(" kg"); //You can change this to kg but you'll need to refactor the calibration_factor
  Serial.println();
}

```

{{< youtube UGawGc2DjWU >}}



* ### Halleffect sensor

{{< youtube JR1X8acbbnw >}}

* ### Ultrasonic sensor

{{< youtube PYfrMGUs6iQ >}}



#### 4.Motor selection

* ### Stepper motor

{{< youtube GAnvpVSDwRo >}}


* ### Servo motor

#### 5.Electronic design

1. ### Design my own final project board

For my final project, I need two hall effect sensors, one stepper motor, one servo motor, one weight sensor and a button. At the begginning I wanted to use serial communication to connect every board and made a very complex diagram for these boards(using TX/RX).But it is so complex for me to do it in two weeks!! I talked to Matti, he gave me a more simple solution. I decide to make communication in one board and with one microcontroller-ATtiny 1614. Because it has many pins. For the ATtiny1614, I got inspirations from the Adrian (http://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#ultrasonic). But depending on my own design, I changed a little bit and add more pins for VCC and GND.

  {{< postimage url = "/images/F4-communication.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/F4-1614-f.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/F4-1614-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/F4-1614.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

   Front  | Back
--------|------
  {{< postimage url = "/images/F4-milling-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/F4-milling.jpg" alt ="throwing moon blocks" imgw ="100%">}}



  {{< postimage url = "/images/F4-stepper motor.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


2. ### Code

* ### Servo motor

```
//Digital fabrication_final project
 //Anna
 //Servo sensor
 //ATtiny1614
 // For the board, "Signal output"-PWM pin connects the pin "PA6"


#include <Servo.h>

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 45;    // variable to store the servo position, rotate the scoop to 45 degree then drives the stepper motor back to the original point.

void setup() {
  myservo.attach(2);  // attaches the servo on pin 2 to the servo object
}

void loop() {

    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position

}

```


* ### Servo motor

```
//Digital fabrication_final project
 //Anna
 //Stepper sensor
 //ATtiny1614
 // For the board, I use "TX" and "RX" to make communication with the stepper motor board and ATtiny1614, but this time I use the same board
 // DIR for the pin-PB1; STEP for the pin-PA7; enable pin for the PB0
#define DIR_PIN          6
#define STEP_PIN         3
#define ENABLE_PIN       7

void setup() {
  
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);
}

void loop() {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(100);
  digitalWrite(STEP_PIN, LOW);
  delay(1);
}

```

* ### Halleffect



```
//Digital fabrication_final project
 //Anna
 //Hall effect
 //ATtiny1614
 // For the board, hall effect connects the pin "PA2"


 int sensorPin = 9;    //analog input pin to hook the sensor to, creates a variable integer called"sensorPin"
 


 void setup() {
 Serial.begin(9600);
  // initialize serial communications,opens serial port, sets data rate to 9600 bps

}

void loop() {
  int value = analogRead(sensorPin);
  // store hall effect sensor reading in value
  
  Serial.printn(value); // print the value

}

```



* ### Final project


```
//ATtiny1614
//Final project
//Inclucing one stepper motor, one servo motor, two hall effect sensors, one button,one weight sensor
//ANNA AND CATS AND M



//For servo motor
#include <Servo.h>
#define SERVO_PIN 2
Servo myservo;  // create servo object to control a servo



// For the weight sensor
#include "HX711.h"

#define calibration_factor -7050.0 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  1
#define CLK  0

HX711 scale(DOUT, CLK);


// For stepper motor
#define DIR_PIN          5
#define STEP_PIN         4



//For halleffect sensor

#define SENSORSTART_PIN  9  //analog input pin to hook the sensor to, creates a variable integer called"sensorPin"

#define SENSOREND_PIN  7

// For button

#define BUTTON_PIN 10




// This is for servo motor to move the scoop up ad down
void scoopUp() {
  myservo.write(0);
  delay(200);
}

void scoopDown() {
  myservo.write(90);
  delay(200);
}



// For detect if scoop close to the edge of the litter box
bool Halleffectsensorclose(int Pin) {
  //if the sensor reads "close to the sensor" it means "true"; it reads "far from the sensor", it means "false"
  // define the vaule for the "distance"
  int value = analogRead(Pin);

  if (value < 100) {
    return true;
  }
  else {
    return false;
  }

}


// detect if cat is there
bool CatIsOut() {
  //if the weightsensor read the value">5kg", it means cats are in, so the stepper motor stops, otherwise it moves
  float value = scale.get_units();

  if (value < 5.0) {
    return true;
  }
  else {


    return false;
  }
}

void motoronestep() {
  //when the cat is out, the stepper motor moves
  if (CatIsOut()) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(100);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(100);
  }

}

void scoopMove(int sensorPin) {
  //when the scopp is far from the halleffect sensor, the motor continue to move

  while ( !Halleffectsensorclose(sensorPin)) {
    motoronestep();
  }

}



// the scoop moves forward if there is no cat, including detect if it is close to the edge of the box
void scoopMoveForward() {
  digitalWrite(DIR_PIN, HIGH); // HIGH means forward, LOW means backward
  scoopMove(SENSORSTART_PIN);

}

void scoopMoveBackward() {
  digitalWrite(DIR_PIN, LOW); // HIGH means forward, LOW means backward
  scoopMove(SENSOREND_PIN);

}




void setup() {
  Serial.begin(9600);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare();  //Assuming there is no weight on the scale at start up, reset the scale to 0
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  myservo.attach(SERVO_PIN);  // attaches the servo on pin 2 to the servo object

}

void loop() {

  int buttonSmartphone = HIGH; // button pin connects PA3
  buttonSmartphone = digitalRead (BUTTON_PIN);

  if (buttonSmartphone == HIGH) {
    scoopMoveForward();
    scoopUp();
    scoopMoveBackward();
    scoopDown();
  }



  // put your main code here, to run repeatedly:

}

```



#### 6.Interface




