+++
author = "Anna Li"
title = "01-Tpoic Research"
date = "2022-01-27"
description = "Basic research for the final project"
tags = [
    "markdown",
    "text",
]
feature_image = "/images/cat litter testing.jpg"
+++

The image is from https://www.litter-robot.com/litter-robot-iii-open-air-with-connect.html

 <!--more-->I think most cat parents have annoying experiences of shoveling shit every day. I have lived with my cats for six years and every morning this is the first thing I need to do after getting up. You can’t hesitate to do it or be lazy to clean the litter, because sometimes it is related to your cats’ health. I used to do it, but when you need to travel or do business for a few days, this thing looks like a bit of a deal. So, for the final project, I would like to make a **cat litter robot**. 

 # Basic research for products on the market

 ## How cat litter robot works

{{< youtube rxP58EX53Ls >}}

<br>




{{< postvideo url = "/video/animation-testing.mp4">}}


{{< postimage url = "/images/testing-results.png" alt ="throwing moon blocks" imgw ="100%">}}

The testing results come from the link:https://www.litter-robot.com/litter-robot-iii-open-air-with-connect.html

{{< postimage url = "/images/price-list.jpg" alt ="throwing moon blocks" imgw ="100%">}}
The price of cat litter robots on Amazon

After testing different brands of litter robot, there are some **key points** being considered.

1. If cat litter robot is flexible for cats to use and adapt to it quickly;
2. If the clean system ensures it is thoroughly cleaned;
3. If the cost is good enough to ensure that it can be used widely;
4. If it ran quietly and quickly, and did not hurt the cats.


More interesting research about the cat litter robot, you may link to: https://www.yourbestdigs.com/reviews/best-automatic-litter-box/

The cat litter robot testing shows **following problems** for the current products on the market:

1. most cat litter robots (self-cleaning cat toilet) are expensive, which is not available for every cat family ;
2. Testing is not that perfect, because of stucking problems for some parts, like cleaning part ;
3. Cats are sensitive or scared to use the litter robot sometimes ;
4. It could not be thoroughly cleaned sometimes ;
5. In case the machine is broken which hurts cat, some machine doesn't have protection system (like camera).

## Why cats don't like litter robot?



[a research repot](https://askmycats.com/cat-wont-use-litter-robot/)

## A research from Fabacademy

A similar project which comes from fabacademy: https://fabacademy.org/2020/labs/agrilab/students/florent-lemaire/projects/final-project-steps, I could get many inspirations from this project.

