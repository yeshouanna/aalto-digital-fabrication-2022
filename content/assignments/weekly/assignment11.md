+++
title = "Week 11: Molding and Casting"
description = "Giving credit to the images used in the demo"
summary = "This week you will explore casting materials available at the lab as well as learn how to use 3D CNC milling to create a custom designed mold."
date = "2022-03-30"
author = "Anna Li"
feature_image = "/images/W11-image.jpg"
+++

# This week's assignment
* Review the safety data sheets for at least two of labs molding and casting materials.
* Make and compare test casts with each of them.
* Design a 3D mold around the stock and tooling that you'll be using.
* Mill it (rough cut + (at least) three-axis finish cut).
* Use it to cast parts.
* Include a hero shot and source files of your design in your documentation.
* Submit a link to your assignment page here.



### This week my plan is to do a scoop part for my final project, comapring 3D printers and 3D CNC milling machine, testing materials and models to decide which way to do the scoop in my final project.

### The molding and casting introduction for this week, The link is here:
### The 3D Milling with MDX-40 introduction video:


##### Knowing the materials of molding and casting

* ### 1-For molding

* #### Carving wax block

 {{< postimage url = "/images/W11-wax.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


Carving wax is a smooth, non-brittle wax designed for carving and/or machining. Although the formulas for most commercial waxes are proprietary, most suppliers will state that hard waxes are some blend of waxes and plastics. This family of waxes has a hardness and consistency of plastic or softer wood. They can be cut or carved with knives, files and rotary or machine tools. To illustrate the usefulness of this type of wax, if one were to get a candle, mount it on a lathe and feed a tool into it, the wax would slough off like butter, stick to the tool and make a mess.

For the carving wax product:


https://www.amazon.com/Freeman-Carving-Block-Sliced-WAX-332-20/dp/B0058EDRBW/ref=sr_1_3?c=ts&keywords=Jewelry%2BMaking%2BWax%2BMolding%2BMaterials&qid=1648640390&s=arts-crafts&sr=1-3&ts_id=12896201&th=1


* #### Cooking the wax in fablab!

{{< youtube 9KfOEuskl5c >}}


* #### Foam block
 {{< postimage url = "/images/W11-foam.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* #### Wood

 {{< postimage url = "/images/W11-wood.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* #### Sika block

This week I will use this material to make a casting

 {{< postimage url = "/images/W11-sika.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


* ### 2-For casting

#### Safety data sheet for casting in Fablab

 {{< postimage url = "/images/W11-safty data sheet for materials.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



 * #### Mold Star Silicones

 Mold Star™ silicones cure to soft, strong rubbers which are tear resistant and exhibit very low long term shrinkage.

 #### I found more informations about Mold Star silicones on the webpage: https://www.smooth-on.com/search/?q=mold+star

* #### What we use this week for casting?

* #### Mold Star 15 Slow

More details about material is by this link: https://www.smooth-on.com/products/mold-star-15-slow/

* #### Mold Star 16 Fast

More details about material is by this link: https://www.smooth-on.com/products/mold-star-16-fast/

* #### Smooth-Cast 305

The Smooth-Cast™ 300 Series of liquid plastics are ultra-low viscosity casting resins that yield castings that are bright white and virtually bubble free.



##### What I want to make this week-Scoop!

#### The scoop is one of the parts of my final project, so this week I want to test one component.

 {{< postimage url = "/images/W11-scoop-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


This what I want to make this week, I use the 3D printer to make a small piece which is one unit for the whole scoop.

 {{< postimage url = "/images/W11-3dmodel.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 #### Then I made a model for milling a wax/sika in Rhino.

* #### A 3D model for CNC milling

 {{< postimage url = "/images/W11-molding model.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 1. main scoop body
 2. the bottom of the scoop surface
 3. one cover 

 {{< postimage url = "/images/W11-molding model-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

* #### A 3D model for silicone model

 {{< postimage url = "/images/W11-silicone mold.jpg" alt ="throwing moon blocks" imgw ="100%">}}


 {{< postimage url = "/images/W11-silicone mold-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

With this silicone mold, the Smooth-Cast 305 liquid could go into the small holes and fills the entire holes, so the function for the cover(No.3) is to make sure the liquid doesn't come out when you shake the box. 


##### Playing with machine

* ### 3D CNC milling machine

We use MDX40 to mill the mold this week which we used in electronic product week, so this small CNC machine could also mill 3D model for molding.

1.  Firstly, setting the milling tools. This week we use end mill (diameter-22mm), end mill(3mm-2 Flute Ball), end mill(6mm-2 flute square).
2. pen V. create a new file and set the parameter. Job size: width/height/thickness(depending on the board size).
3. Flatten the surface of the board. You need to make sure one side is flat. Using the “End Mill 22mm”. 
Parameter- Cut Depth: 10mm; Tool selet: End Mill(22mm); Pass Depth:2mm; Stepover: 8.8mm  40%.
4. The calculation for the “Feed Rate” depends on the materials. This week I used sika, so the “feed rate” here is different. We could use the material **Brass,Copper, Bronze** .Spindle Speed is 14000 r.p.m; Fr=nf*cl*ss, For rough finnishing tool, the diameter is 6mm, the **Feed Rate** here is 2*0.05*14000=1400 mm/min; Plunge rate is Fr/4.
This is very important to check the table to see what is **cl** we use, to avoid to break the machine tool.


 {{< postimage url = "/images/W11-table.jpg" alt ="throwing moon blocks" imgw ="100%">}}

#### The milling process

{{< youtube MAekWbYOd9k >}}

1. Loading the material

 {{< postimage url = "/images/W11-load the material.jpg" alt ="throwing moon blocks" imgw ="100%">}}

2. Setting the tool

  {{< postimage url = "/images/W11-flatten tool.jpg" alt ="throwing moon blocks" imgw ="100%">}}

3. Flatten the surface

    {{< postimage url = "/images/W11-flatten the surface-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/w11-flatten the surface.jpg" alt ="throwing moon blocks" imgw ="100%">}}

4. Rough finishing


    {{< postimage url = "/images/W11-First step.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-rough finnishing.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-First step-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

5. End finishing


    {{< postimage url = "/images/W11-first step-11.jpg" alt ="throwing moon blocks" imgw ="100%">}}

6. Final mold

    {{< postimage url = "/images/W11-final milling.jpg" alt ="throwing moon blocks" imgw ="100%">}}



#### Coasting

* #### Choosing materials
"mold-star fast"/" mold-star slow"
mold-star slow: better regin, better for tuning and with the bigger mold, the pot life is 50 mins; Thinner to make the mixing and pouring smoothly

    {{< postimage url = "/images/W11-material.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-thinner.jpg" alt ="throwing moon blocks" imgw ="100%">}}

* #### Measure the volume of the mold

    {{< postimage url = "/images/W11-volume.jpg" alt ="throwing moon blocks" imgw ="100%">}}

pour some water in the mold 

* #### Measure the weight of liquid(how much the silicon should fill the cup afterwards)
1. Scale the cup first 0.0 with the cup(-7.09g)
2. Pour the water into the cup, and measure the weight

    {{< postimage url = "/images/W11-cup.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-cup (1).jpg" alt ="throwing moon blocks" imgw ="100%">}}


* #### Preparing for the silions and silicon thinner

Mix the silicon with a clean stick firstly

    {{< postimage url = "/images/W11-silicons-mix.jpg" alt ="throwing moon blocks" imgw ="100%">}}

* #### Vaccum chamber to remove the bubbles inside the liquid

    {{< postimage url = "/images/W11-bubble.jpg" alt ="throwing moon blocks" imgw ="100%">}}

Notes: 
Meter goes to until red which means enough depressure rised. Then let the air out slowly and the meter goes to the original pressure. Making this process at least three times until the bubble almost gets out. 

* #### Pouring the mixed silicon into the mold

    {{< postimage url = "/images/W11-coasting-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-coating-2.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-coasting-3.jpg" alt ="throwing moon blocks" imgw ="100%">}}

* #### Another piece of the  mold and casting

    {{< postimage url = "/images/W11-mold-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-casting-11.jpg" alt ="throwing moon blocks" imgw ="100%">}}

    {{< postimage url = "/images/W11-casting-12.jpg" alt ="throwing moon blocks" imgw ="100%">}}