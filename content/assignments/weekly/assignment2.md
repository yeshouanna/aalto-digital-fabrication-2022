+++
title = "Week 02:Principles and Practices"
description = "Giving credit to the images used in the demo"
summary = "welcome to deeper study for webpage-hugo and scss"
date = "2022-01-17"
author = "Anna Li"
feature_image = "/images/hugo.png"
+++

## Hugo
Hugo is one of the most popular open-source static site generators. There are many themes which you could download and modify your own webpage, and this week I am struggling with the theme I download. It is better to come back to HTML basics to understand the structure and logic. But I am happy I learned a lot with solving different problems. And my webpage seems like a manageable solution and at least someting is already in the track. Don't hesitate the energy of a beginner and don't hesitate to ask help from masters and your classmates!
### Install hugo
{{< postimage url = "/images/install hugo.png" alt ="throwing moon blocks" imgw ="100%">}}
Open the homepage of **Hugo**, and click the **GitHub**; 
{{< postimage url = "/images/install-hugo-1.png" alt ="throwing moon blocks" imgw ="100%">}}
You will find the latest **release** on the right side, click it;
{{< postimage url = "/images/install hugo-2.png" alt ="throwing moon blocks" imgw ="100%">}}
Because mine is Windows, so I picked the windows-64. I have small problems here, when I installed **"hugo_0.92.1_windows_ARM64.zip"**, it showed not compatible with my systems. So I picked **"hugo_0.92.1_Windows-64bit.zip"**.If you want use hugo themes, you could download extended version, because extended is required by some nicer hugo themes.
When you download the hugo file, it is placed on the default path. It is better to move it in the folder which is your terminal processpr, like.\cmder\vendor\bin, which ensures hugo can be runned successfully.
{{< postimage url = "/images/install hugo-3.png" alt ="throwing moon blocks" imgw ="100%">}}
For **"hugo version"**, you could find the location of your hugo and see if it is the current hugo you want to use; For **"hugo serve"**, you could run the hugo to see your static webpage.
