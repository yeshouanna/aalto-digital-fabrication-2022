+++
title = "Week 05:Computer-Controlled Cutting"
description = "Giving credit to the images used in the demo"
summary = "This week we will cover the basics of computer-controlled cutting which in the context of this course is vinyl and laser cutting."
date = "2022-02-11"
author = "Anna Li"
feature_image = "/images/W5-lasercutting.jpg"
+++

The image is from etsy.com

## This week's assignment
* Characterize your lasercutter's focus, power, speed, rate and kerf.
* Cut something on the vinyl cutter.
* Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple ways.
* Document all of the points above in a new page on your documentation website.
* Submit a link to the assignment page. 

## My goal in this week is to make a cat toy by laser cutter and a **USP** sticker on the clothes.(**USP** means urban studies and planning which is actually where I came from.)

# **Be patient in the fablab! Test and Play!**
 {{< postimage url = "/images/W5-fablab.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

## **1-To make a good T-Shirt sticker- start with Vinyl cutter.**
To start the Vinyl cutting machine tutorial, you may use this Youtube video.

{{< youtube FFKc8PFgNHg >}}

* ### Testing-1
This is Aalto Fablab Vinyl cutter.

 {{< postimage url = "/images/W5-cutting.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

1. There is an x-axis so you could move the blade from left to right, and there is also blade mounted there. You could replace the blade easily. 

   Checking the cutter machine | Replace the blade
--------|------
  {{< postimage url = "/images/W5-settingup.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W5-blade.jpg" alt ="throwing moon blocks" imgw ="60%">}}

2. Start up the machine and loading the materials

Be careful to check the materials! I I made a mistake in the material which caused me not to be able to print on the clothes in the end(I am laughing at myself here...)

   Cutting domain | Start up
--------|------
  {{< postimage url = "/images/W5-domain.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W5-.jpg" alt ="throwing moon blocks" imgw ="60%">}}

 **If the material is too small, sometimes the machine will show "unloading" or “wrong position".**

 1. Putting the sheet which you want to print under the blade. And press `Menu`for loading and select sheet, like **Roll**, **Piece**, and **Edge**. And for the small piece, I recommond the "Piece" or "edge". 
 Now Loading...

 2. Afterwards you could get a **size** on the screen, like W: 66 mm
              L: 437 mm
 3. Parameters setting. For **force**, you could press: `Force`. **if cutting copper, you need to transfer firstly before printing, because here the material is easy to be hurt( the material is soft enough). So the speed is needed to increase to 10m/s.**

 4. If everything is done, you could print. 

 5. When it is done, you could press `Menu`-> `Unsetup`-> `Enter`.

 **Materials**
 
   Cutting domain | Materials
--------|------
  {{< postimage url = "/images/W5-cuttingdomain.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W5-materials.jpg" alt ="throwing moon blocks" imgw ="60%">}}


  Heat Transfer | Standard Sticky Vinyl
--------|------
  {{< postimage url = "/images/W5-stickerclothes.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-commonsticker.jpg" alt ="throwing moon blocks" imgw ="50%">}}

   Standard Sticky Vinyl | Copper Vinyl
--------|------
  {{< postimage url = "/images/W5-stickerclothes-1.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-copper vinyl.jpg" alt ="throwing moon blocks" imgw ="75%">}}

   Clean your sticker with tweezer | Using tape to transfer
--------|------
  {{< postimage url = "/images/W5-clean your sticker.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-using taper to transfer.jpg" alt ="throwing moon blocks" imgw ="75%">}}





3. Turn to the heater to make your own sticker on the cloth.

I want to print a **USP** on this clothes, which means **Urban Studies and Planning**.

 {{< postimage url = "/images/W5-heatermachine.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

1. Firstly, to start up the machine. The temperature on the screen is current temperature. The red button on the side is the ""starting button"".
2. Press the two is the **target temperature** you want, Press third time is the **time** you want to heat, so normally **5 seconds** is fine. The target temperature here is around 130.
3. Waiting for the machine to heat itself, maybe you need some patience, it needs some time. Then you could test the board temperature like what I did. 
4. Checking if the material is right or if the sticker is hear transfer. It is important!

 {{< postimage url = "/images/W5-currentt.jpg" alt ="throwing moon blocks" imgw ="50%">}} 
The current temperature is **93**. 

 {{< postimage url = "/images/W5-target.jpg" alt ="throwing moon blocks" imgw ="50%">}} 
 The target temperature Which I want to heat is 130.

{{< postimage url = "/images/W5-time.jpg" alt ="throwing moon blocks" imgw ="50%">}} 
The time is about ""10 seconds"", but you could change the time.

 {{< postimage url = "/images/W5-testing-1.jpg" alt ="throwing moon blocks" imgw ="50%">}} 
 You could test the temperature with this tool. 

  {{< postimage url = "/images/W5-press.jpg" alt ="throwing moon blocks" imgw ="50%">}} 
 Putting the clothes on the board to heat.


### Testing Fail!!!

   {{< postimage url = "/images/W5-wrong.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

   {{< postimage url = "/images/W5-wrong-1.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

## The problem here is that I mistake the materials, I used "standard sticky vinyl" but not "heat transfer" here, they really look similar for this colour.




## **2-To make a cat toy - start with Laser cutting.**

To start the Laser cutting, you may follow this tutorial.But this is our old machine, for this week we practice with the new machine in Aalto University fablab.

{{< youtube BRWeJ3ehauI >}}




### **This is the new laser cutter in our Fablab**
 {{< postimage url = "/images/W5-new machines.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

### First thing you need to do before cutting. 

1. Make sure the ventilation is open! The ventilation button is on the wall, so you need check especially you are the first one to use this machine in the morning.

2. Starting up the machine with the key! You could ask from our tutor in the Fablab. 

3. Start the "Illustrator" on the computer desk and inport your cutting file. 

### The ventilation (make sure it is open)
 {{< postimage url = "/images/W5-ventilation.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

### The key for opening the machine

 {{< postimage url = "/images/W5-keys.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

### The machine will check if there is people nearby for every few minutes, so when it warns (normally with a sound), you need to press on the left side button (or put the card on the screen). When everything is done, you need to closed the machine with the key and also press the right button for stop.

 {{< postimage url = "/images/W5-using card.jpg" alt ="throwing moon blocks" imgw ="50%">}} 


#### I want to cut a cat toy this week, so the first thing I want to do is testing the materials and curve. Testing if everything is good before you cut your final file. I would say it will take a long time to test the fitable parameter depending on different materials.Your could see my feature image, and that is what I want to do this week.


* Firstly, I make a model and drawing in Rhino and export with 1:1 sacle to Illustrator. I need to test the **vector** and **engrave** on the board, so firstly I need to test with smaller piece.

 {{< postimage url = "/images/W5-finalcutting .png" alt ="throwing moon blocks" imgw ="100%">}} 

* Secondly, testing with the small piece. 

1. open the **Illustrator** and open the file "laser cutting". Giving the stroker： **0.01mm**.

 {{< postimage url = "/images/W5-stroker.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 2. Because the "cat" is for engraving, so I need give a shape for the **lines** on cat's face. Choosing the lines, then go to the `Object`-> `Path`-> `Outline Stroker`.

  {{< postimage url = "/images/W5-outline.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 3. Selecting the all the shapes and lines on the black cat, then go to `Window`-> `Pathfinder` to give the final shape for engraving.

  {{< postimage url = "/images/W5-pathfinder.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  4. Go to the `File`-> `Print`, then everything is done in Illustrator until now. **Media size** is **Custom**. **Print layers** is **Visible&Printable Layers**, other parameters are default. Then hit **Print**. The file is sent to the **Epilog Laser Job Manager** directly.

  {{< postimage url = "/images/W5-print.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W5-print-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


3. Open the **Epilog Laser Job Manager**, Go to **Jobs**, then you will find the current file which be sent just now. Wait for a while then you may see the camera, normally when you first use the machine the camera works. But these days because of the fablab network setting, sometimes you can not see the camera. You probably **restart** the machine with the key. Hopefully it will be solved in the future. 

After you saw the camera, you may give the layout for the cutting ones. But I recommond you give a good layout for your cutting, which is better to put them intensivly and in the coner. Because I found sometimes it could not be cut 100%. In case you may not waste the material, so you should give a good layout before cutting.

 {{< postimage url = "/images/W5-camera.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

4. Testing with different materials and parameters. I would say it would take a long time far from you think sometimes, because it is not easy to give a good cutting with default speed and power from machine. 
So you may see clearly about the engraving part and vector cutting one on the right side. 

 {{< postimage url = "/images/W5-jiemian.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

**Materials testing**


   Plywood test | Acrylic test
--------|------
  {{< postimage url = "/images/W5-materialstesting.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-acrylic.jpg" alt ="throwing moon blocks" imgw ="70%">}}

**Parameters testing**

   Vector/Speed  | Vector/speed
--------|------
  {{< postimage url = "/images/W5-parameters.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-speed20.jpg" alt ="throwing moon blocks" imgw ="70%">}}


   Resolution-300  | Resolution-500
--------|------
  {{< postimage url = "/images/W5-resolution300.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W5-resolution500.jpg" alt ="throwing moon blocks" imgw ="70%">}}

5. Testing the joints, make sure if it is fit very well for each piece.

 {{< postimage url = "/images/W5-joints.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


 6. If everything is fine, then go to the final cutting. Now I will finish my cat toy!

 {{< postimage url = "/images/W5-cattoy.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


 7. Testing the toy with my cats!

  {{< postimage url = "/images/W5-final.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W5-final-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 





