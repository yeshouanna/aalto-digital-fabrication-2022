+++
title = "Week 13：Machine Building"
description = "Giving credit to the images used in the demo"
summary = "This week you will build a mechanism and automate it using off-the-shelf motor driver solutions."
date = "2022-04-20"
author = "Anna Li"
feature_image = "/images/W13-image.jpg"
+++

# This week's assignment
* Design a machine that includes mechanism, actuation and automation.
* Build the mechanical parts and operate it manually.
* Actuate and automate your machine.
* Document the group project and your individual contribution.
* Include a hero shot and link to machine repository in your documentation.
* Submit a link to your assignment page here.




### This week I mainly focus on mechanical movement and 3D printing for the components of the machines. There are many solutions to drive the motor following one linear axis, so during these two weeks, we peers discussed a lot about the mechanics part and how to improve the design. Thanks to my group members, they help me a lot with my final project. I have great progress on my final project and I also feel many things need to be improved afterward, like how to raise the speed of the linear movement, how to make the machine lighter(now the machine structure is heavy), how to collect the “poop” in the end, how to clean the robot…..The whole group work includs mechanics part, programming part, interface part, so I will learn from other peers about the programming and interface part after this week, and learn to test the machine by myself for my final project.

### So firstly, I start to know more about the mechanical design from the fab academy.
A page with many links to valuable resources regarding mechanical design.
http://academy.cba.mit.edu/classes/mechanical_design/index.html

http://academy.cba.mit.edu/classes/machine_design/index.html


* ### Timing belts and pulleys
* ### steel ball bearings
* ### gears
* ### mounted roller bearings

Roller bearings have a thinner profile and larger contact area than ball bearings, making them stronger and more space-efficient than ball bearings.
Sealed bearings block out dust and contaminants.

  {{< postimage url = "/images/W13-machine-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W13-machine-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


##### The Machine Design part

{{< youtube mWaOvfo9lvk >}}

* ### 1-Making the machine works firstly, starting with cardboard,during the whole progress, Karl helped a lot. Yuhan and I tried to use the tools in Fablab to assemble basic movement. you could see the process for testing in our final video.

  {{< postimage url = "/images/W13-start.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


Making a scoop box with gear and motor, hopefully it could rotate at the first time.

  {{< postimage url = "/images/W13-scoop.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

The first layout for out cat litter robot.

  {{< postimage url = "/images/W13-first layout.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* ### 2-Model making

  {{< postimage url = "/images/W13-model.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W13-model-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

At this stage, I find the software is important.For example, usually I used Rhino which is good for setting the parameters with grasshopper (the plugin in Rhino). But sometimes it is difficult to snap the point when you download a mesh model, because the curves you draw in Rhino are **Nurbs** . I feel it is not that precise for snaping the point which makes it difficult for 3D printing. I almost took some time to fix the 3D models when I assembled them. I think I might change the software afterwards for my final project. 

  {{< postimage url = "/images/W13-connecton badly.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

You could see these red cicles, when I want to snap the points here( because the models I download here is mesh), it is very difficult. So I find some gaps between different components. 



 {{< postimage url = "/images/W13-worm gear.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/W13-motor.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



* ### 3-Aseembling the machine and Testing with Yuhan

The programming and interface are from Samuli in our group, it works very well for the code. In the fablab, Yuhan is mainly responsible for testing the code which Samuli write, also improve our interface. I helped to test, but I need to learn to test by myself afterwards for my final project. I would say that is also interesting part, sometimes programming works, electronic works, but mechanics doesn't work. That's what we suffered most time for this assignment.
Especially there is cerf for the 3D printing.

The testing process is in our group video.













 













