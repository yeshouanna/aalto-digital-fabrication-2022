+++
title = "Week 07: 3D Scanning and Printing"
description = "Giving credit to the images used in the demo"
summary = "This week we are going to learn about 3D printing design rules that will help you to design better objects for 3D printing. We will also cover the basics of 3D scanning."
date = "2022-02-03"
author = "Anna Li"
feature_image = "/images/lion.jpg"
+++

## This week's assignment
* Test the design rules of the 3D printers at Aalto Fablab.
* Design an object that can not be made using subtractive manufacturing.
* 3D print the object you designed.
* 3D scan something and (optionally) print it.
* Document your process in a new page on your website.
* Submit a link to the page on your documentation website.

### This is one of my favourite weeks! except documentation part, because there are many explorations for me:)

#### This week I want to explore the limitations of the machine and test materials.

## 1. 3D Scanning

#### 1.1 Scanning

 {{< postimage url = "/images/w7-scsan6.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/W7-scsan.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/w7-scan1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/w7-scan2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/w7-scan3.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/w7-scsan5.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/w7-scan4.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

#### 1.2 Importing the model

*  Import from Leo

{{< postimage url = "/images/w7-import.png" alt ="throwing moon blocks" imgw ="100%">}} 

*  Connect by IP
{{< postimage url = "/images/W7-Ip.png" alt ="throwing moon blocks" imgw ="100%">}} 

*  Import the file
{{< postimage url = "/images/step1-messy.png" alt ="throwing moon blocks" imgw ="100%">}} 

*  Changing to the **Orthogonal View** which makes erasing more easier
{{< postimage url = "/images/step4-messy.png" alt ="throwing moon blocks" imgw ="100%">}} 

*   **Lasso selection** to erase
{{< postimage url = "/images/step2-messy.png" alt ="throwing moon blocks" imgw ="100%">}} 

*   Selecting areas to erase
{{< postimage url = "/images/step3-messy.png" alt ="throwing moon blocks" imgw ="100%">}}

*   Registration
{{< postimage url = "/images/W7-registration.png" alt ="throwing moon blocks" imgw ="100%">}}

*   Align
{{< postimage url = "/images/W7-alin.png" alt ="throwing moon blocks" imgw ="100%">}}

*   Export scans
{{< postimage url = "/images/W7-export.png" alt ="throwing moon blocks" imgw ="100%">}}

### Scanning yourself! Thanks my peer Katie!


{{< youtube crnm0E3Djp8 >}}

   Before | After
--------|------
  {{< postimage url = "/images/W7-model.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W7-final.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  ## 2. 3D Printer

### This week I have made many models to test the materials and the machine! knowing more about the machine then find best way to use it is an interesting journey this week. 

### * Testing the colors, I use different extruders to set different colors for 3D printer. I want to make a Chinese checker this week, so I need to print many checkers with different colors. But to my surprise, the base layer doesn't seem to print according to the set extruder, even though a different extruder is set before printing starts.

{{< postimage url = "/images/W7-color-2.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W7-color-3.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W7-color-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}


{{< postimage url = "/images/W7-color-5.jpg" alt ="throwing moon blocks" imgw ="100%">}}


### * 3D printing test_ lion with hair
This is an interesting exploration for the 3D printer this week. I feel the model someone made is really smart, which make use of the machine trace also know the limitation of the materials here.

{{< youtube f4YotHD_iFU >}}

