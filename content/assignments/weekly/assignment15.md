+++
title = "Week 15：Networking and Communication"
description = "Giving credit to the images used in the demo"
summary = "This week you will learn how to establish communication between two or more PCBs that you made."
date = "2022-05-02"
author = "Anna Li"
feature_image = "/images/W15-diagram of the board.jpg"
+++

# This week's assignment
* Design, build, and connect wired or wireless node(s) with network or bus addresses.
* Send a message between two or more projects.
* Include a hero shot and source files of your board in your documentation.
* Describe what you learned about networking on your documentation website.
* Submit a link to your assignment page here.


#### Reference link from Fab Academy: https://vimeo.com/703833815




### This week my goal is to understand the basic logic of networking and communication, at the same time, establish communication between my two boards. I want to test how to make communication between the PIR sensor and stepper motor, also for my final project. 


##### Basic Knowledge of Networking and Communication

There are many types of networking procedures that could be used to communicate between various microcontrollers, in order to widen the cpabilities of a certain system. Among those networking methods are: **Serial Communication** which is mainly communicating data using the serial connection between microcontrollers. **I2C** which mainly using the SDA and SCL pins to communicate between microcontrollers. **SPI** which uses the MISO and MOSI pins (ISP pinouts) to communicate between microcontrollers.


* Wired communication
   1. Asynchronous communication
      * RX/TX
      * V-USB
   2. Synchronous communication
      * 12C
      * SPI

#### Asychronous vs Synchronous

"Asynchronous" (not synchronous) means that data is transferred without support from an external clock signal. This transmission method is perfect for minimizing the required wires and I/O pins, but it does mean we need to put some extra effort into reliably transferring and receiving data. 

"Synchronous" data interface always pairs its data line(s) with a clock signal, so all devices on a synchronous bus share a common clock. This makes for a more straightforward, often faster transfer, but it also requires at least one extra wire between communicating devices.


   Asychronous | Synchronous
--------|------
  {{< postimage url = "/images/W15-asy.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W15-syn.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  (the images are from https://fabacademy.org/2019/labs/barcelona/local/clubs/electroclub/networking/)


#### What is bus network? 

{{< postimage url = "/images/W15-bus network.png" alt ="throwing moon blocks" imgw ="70%">}}

A bus network is a local area network (LAN) topology in which each node -- a workstation or other device -- is connected to a main cable or link called a bus. I don't want to explore that deep for this term, but it is really good to know every term and know how they work for the whole networking.


#### Asynchronous communication

We use the asynchronous communication when data can be sent without timing constraints as well as oftenly less speed, with the benefit of using one less wire, one less port one each side.

* ### RX/TX

By RX/TX we know the most common way of serial communication. It requires only two wires, appart form a common ground:(the receiver,**RX**, and the transmitter, **TX**)

{{< postimage url = "/images/W15-rxtx.jpg" alt ="throwing moon blocks" imgw ="70%">}}

We are used to hooking up VCC to VCC, GND to GND, MOSI to MOSI, etc. But it makes sense that **the transmitter should be talking to the receiver, not to another transmitter. So it is important to think about it.**

* ### Data

Data is sent asynchronously, generally, they are grouped by bytes, with some extras, like parity bits or and synchronisation bits(start and stop).Usually, an asynchronous serial communication frame consists of a START bit(1bit) followed by a data byte(8 bits) and then a STOP bit(1 bit), which forms a 10-bit frame. The frame can also consist of 2 STOP bits instead of a single bit, and there can also be a PARITY bit after the STOP bit.

{{< postimage url = "/images/W15-data byte.jpg" alt ="throwing moon blocks" imgw ="100%">}}

More details, about this reference: https://learn.sparkfun.com/tutorials/serial-communication/all


#### 12C
The Inter-integrated Circuit (I2C) Protocol is a protocol intended to allow multiple slave digital integrated circuits (chips) to communicate with one or more master chips. It is only meant for short distance communications within a single device. I2C is a convenient way of setting up communication because:

* It allows for several masters
* It allows for several slaves
* The number of wires does not change

#### should give more learning about 12C?







#### Learning Reference

* Serial communication : https://learn.sparkfun.com/tutorials/serial-communication/all

* Basic knowledge about Networking and communication: https://fabacademy.org/2019/labs/barcelona/local/clubs/electroclub/networking/

* Understanding electronics: https://fabacademy.org/2019/labs/barcelona/local/clubs/electroclub/electronics2/

* Networking and communication Workshop: https://hackmd.io/@fablabbcn/B15x5dn9V?type=view#Overview

* Other documentaries from Fablab: 
1. http://academany.fabcloud.io/fabacademy/2021/labs/kannai/site/group_assignments/week13/#connect-esp-board-with-the-mobile-phone-with-wi-fi-succeeded

2. Sending messages between two projects,  https://fabacademy.org/2021/labs/berytech/Networking_and_Communication.html

* 12C : https://learn.sparkfun.com/tutorials/i2c


##### Connect wired or wireless node(s) with bus address

* ### Using online Arduino simulator_WOKWI

My feeling for using WOKWI to creat a sketch is really hard at the begining, because I have no more experience for programming....The good thing is that they have many useful simulator examples, so starting from this point. 

So firstly, I also find a good documentary to see how to do it. 

#### Documentary reference(if know more details,these links are also useful)

1. https://fabacademy.org/2019/labs/leon/students/elena-cardiel/week15.html

2. https://archive.fabacademy.org/archives/2016/deustofablab/students/60/assignment15.html



* I used the simulator to see how to address and make action for 3 devices, but now I could not add another Arduino and RX/TX, so for the coding simulation part, it seems that it happened separately. But I understand how to address different diveces with codes.

* What I want to test is that if I could make connection between sensor board and motor board!

{{< youtube Sq4P13VaSrY >}}


{{< postimage url = "/images/W15-txrx.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W15-binary.jpg" alt ="throwing moon blocks" imgw ="100%">}}




* #### CODE

```
// These are our addresses for 3 devices
#define ADDR_ledA 0b01     // 00000001
#define ADDR_stepper1 0b00   // 00000000
#define ACT_ON 1 
#define ACT_OFF 0 
#define ADDR_pir1 0b10 // 00000010

int led = 13;                // the pin that the LED is atteched to
int sensor = 2;              // the pin that the sensor is atteched to
int state = LOW;             // by default, no motion detected
int val = 0;                 // variable to store the sensor status (value)
int stepper = 3;                 // stepper pin


void setLED(byte address, byte action)
{
int led_pin;
if(address == ADDR_ledA){
  led_pin= led;
}

else if(address == ADDR_stepper1){
  led_pin= stepper;
}
else if(address == ADDR_pir1){
  led_pin= sensor ;
}

bool led_state;
if(action == ACT_ON){
  led_state = HIGH;
}
if(action == ACT_OFF){
  led_state = LOW;
}

if(address == ADDR_pir1){
  delay(3000);
int val = digitalRead(led_pin);
if (val == HIGH) {
   Serial.println("Motion detected!"); 
}

else if(val == LOW) {
   Serial.println("Motion stopped!"); 
}

}


else{
digitalWrite(led_pin, led_state);
}
 
}

void setup() {
  pinMode(led, OUTPUT);      // initalize LED as an output
  pinMode(sensor, INPUT);    // initialize sensor as an input
  Serial.begin(9600);        // initialize serial
  Serial.println("Welcome");


  // 1 and 0 in binary

  Serial.print(ADDR_ledA, BIN);
  Serial.println("< ADDR_ledA ");

  Serial.print(ADDR_stepper1, BIN);
  Serial.println("< ADDR_stepper1 ");


  // how bit shifting works


}

void loop(){



// how the shifting works here

  if(Serial.available()){
  byte incoming = Serial.read();

  byte address = incoming & 0b00000011;
  Serial.print(address,BIN);
  Serial.println(" address ");
  
  byte action = (incoming & 0b00000100) >> 2;
  Serial.print(action,BIN);
  Serial.println(" action ");
  setLED(address,action);
  }
  

}

```


* ### Connecting the different devices from INPUT and OUTPUT week!

* Now what I want to do is using "Hello-ATtiny412" and "Halleffect sensor" to test the programming 


{{< youtube b_FXsk14Ac4 >}}


{{< youtube JR1X8acbbnw >}}




##### My Final Project


For my final project, my PCB board didn’t work after the stepper motor test. For the whole code, I think they worked very well, except the weight sensor to detect if the cat was there, it seemed a bit late for testing, which means it couldn’t give a quick command when the cat was there, and the stepper motor stopped working. I had many videos for testing this working experience. 


  **The code**


```
//Ardunio uno
//Final project
//Inclucing one stepper motor, one servo motor, two hall effect sensors, one button,one weight sensor
//ANNA AND CATS AND M



//For servo motor
#include <Servo.h>
#define SERVO_PIN1 6
#define SERVO_PIN2 9
Servo myservo1;  // create servo object to control  servo motor 1
Servo myservo2;  // create servo object to control  servo motor 2


// For the weight sensor
#include "HX711.h"

#define calibration_factor 108500 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  2
#define CLK  3

HX711 scale(DOUT, CLK);


// For stepper motor
#define DIR_PIN          5
#define STEP_PIN         4

#define DIRFORWARD LOW
#define DIRBACKWARD HIGH

//For halleffect sensor

#define SENSOR_PIN1 A1  //analog input pin to hook the sensor to, creates a variable integer called"sensorPin"
#define SENSORTARGETVALUE1 850

#define SENSOR_PIN2  A2
#define SENSORTARGETVALUE2 600

// For button

#define BUTTON_PIN 10



// This is for servo motor to move the scoop up ad down
void scoopUp() {
  myservo1.write(0);
  myservo2.write(180);
  delay(2000);

}

void scoopDown() {
  myservo1.write(90);
  myservo2.write(90);
  delay(2000);
}



// For detect if scoop close to the edge of the litter box
bool Halleffectsensorclose(int Pin, int TargetValue) {
  //if the sensor reads "close to the sensor" it means "true"; it reads "far from the sensor", it means "false"
  // define the vaule for the "distance"
  int value = analogRead(Pin);
  value = map(value, 0, 1024, 1024, 0);
  //Serial.print(" halleffectsensor ");
  //  Serial.print(Pin);
  //  Serial.print(" ");
  //
  //  Serial.print(TargetValue);
  //  Serial.print(" ");
  //
  //  Serial.println(value); // print value to Serial Monitor


  if (value > TargetValue) {
    return true;
  }
  else {
    return false;
  }

}


// detect if cat is there
bool CatIsOut() {
  //if the weightsensor read the value">2kg", it means cats are in, so the stepper motor stops, otherwise it moves
  float value = scale.get_units();
//    Serial.print(" weightsensor ");
//    Serial.print( value ); //scale.get_units() returns a float
//    Serial.print(" kg"); //You can change this to kg but you'll need to refactor the calibration_factor
//    Serial.println();

  if (value < 1.0) {
    return true;
  }
  else {

    return false;
  }
}

void motoronestep() {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(50);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(50);
}

void scoopMove(int sensorPin, int TargetValue) {
  //when the scopp is far from the halleffect sensor, the motor continue to move
  while ( !Halleffectsensorclose(sensorPin, TargetValue) && CatIsOut()) {
    //when the cat is out, the stepper motor moves
    motoronestep();
  }
}



// the scoop moves forward if there is no cat, including detect if it is close to the edge of the box
void scoopMoveForward() {
  digitalWrite(DIR_PIN, DIRFORWARD); // Look at the define
  scoopMove(SENSOR_PIN1, SENSORTARGETVALUE1);

}

void scoopMoveBackward() {
  digitalWrite(DIR_PIN, DIRBACKWARD); // Look at the define
  scoopMove(SENSOR_PIN2, SENSORTARGETVALUE2);

}


//function for testing stepper motor and scoop
void teststepper() {
  scoopDown();
  int steps = 4000;
  digitalWrite(DIR_PIN, DIRFORWARD);
  for (int i = 0; i < steps; i++) {
    motoronestep();
  }
  digitalWrite(DIR_PIN, DIRBACKWARD);
  for (int i = 0; i < steps; i++) {
    motoronestep();
  }
  scoopUp();
}




void setup() {
  Serial.begin(9600);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare();  //Assuming there is no weight on the scale at start up, reset the scale to 0
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  myservo1.attach(SERVO_PIN1);  // attaches the servo on pin  to the servo object
  myservo2.attach(SERVO_PIN2);  // attaches the servo on pin  to the servo object
}


void loop() {




  //int buttonSmartphone = HIGH; // button pin connects PA3
  //buttonSmartphone = digitalRead (BUTTON_PIN);

  //if (buttonSmartphone == HIGH) {
  if (true) {
    scoopMoveForward();
    scoopUp();
    scoopMoveBackward();
    scoopDown();
  }



  // put your main code here, to run repeatedly:

}

```

I got help from my boyfriend for the programming part, so we tested my machine together. There are many interesting tests for the different small parts for the code, and I will also share in this part. I think the most interesting part is to find the same direction of the movement  for the servo motor. Because I made the scoop fixing with the two servo motors, and I needed to make sure they have the same direction for movement. And also, I need to make sure they could start to move at the same point but they are in the mirror, for example, one servo motor starts from “0”; the other one starts from “180”. But it took some time to get the right movement. 



```
// This is for servo motor to move the scoop up ad down
void scoopUp() {
  myservo1.write(0);
  myservo2.write(180);

```

During this process, I also met the problem of “wire connection”. The wires make everything complex, and I need more wire connections with tapes. So there are some tests that are just for connection, even though I need to improve my design afterward, I already found the programming part took more time than I thought. 


{{< youtube LlK2vguksc0 >}}

At the beginning, the servo motors gave different directions for movement.

{{< youtube FLQHIGtk_Ao >}}


 Testing the code(HIGH/SCOOP BACKWARD/SCOOPDOWN), I forgot to mute the video, but it was funny and I felt programming and testing would take a long time from then on. 


{{< youtube bp1N6uLXf0Q>}}


Testing the hall effect sensor with stepper motor, and servo motor. They worked very well!


{{< youtube 2IkEGKRSSqM >}}

Testing my Attiny1614 board and TMC2208,but unfortunately, after testing for a while, it didn’t work anymore. Then I decided to use Arduino Uno this time.

{{< youtube SC4OcXI3Mck>}}




